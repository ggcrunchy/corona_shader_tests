--- Heat shader scene.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Standard library imports --
local ipairs = ipairs
local random = math.random
local sin = math.sin

-- Modules --
local effect_props = require("corona_shader.effect_props")
local utils = require("utils")

-- Corona globals --
local display = display
local Runtime = Runtime

-- Corona modules --
local composer = require("composer")

-- Heat scene --
local Scene = composer.newScene()

-- --
local CX, CY = display.contentCenterX, display.contentCenterY

-- --
local Groups = {}

-- --
local EnterFrame

-- Create Scene --
function Scene:create ()
	--
	local function AuxHeat (func)
		return function (group, name, kernel)
			local image = display.newImageRect(group, "Image1.jpg", 256, 256)

			image.x, image.y = CX, CY
			image.fill.effect = name

			if func then
				func(image)
			end

			-- Add some controls.
			utils.SliderAndText_Kernel(image, 10, "extend", kernel)
			utils.SliderAndText_Kernel(image, 40, "frequency", kernel)
		end
	end

	-- Update the current group.
	function EnterFrame (event)
		for _, group in ipairs(Groups) do
			if group.isVisible and group.m_func then
				group.m_func(event)
			end
		end
	end

	-- Effect selection.
	utils.TabBar_Triples({
		-- Effect 1 --
		"Normal", "filter.heat.basic", AuxHeat(),

		-- Effect 2 --
		"Snapshot", "filter.heat.basic", function(group, name, kernel)
			local snap = display.newSnapshot(300, 300)

			group:insert(snap)

			snap.x, snap.y = CX, CY

			-- Put some stuff in the snapshot.
			local red = display.newRect(snap.group, 80 - CX, 90 - CY, 100, 200)

			red:setFillColor(1, 0, 0)

			local BlueY = 250 - CY

			local blue = display.newRect(snap.group, 160 - CX, BlueY, 100, 200)

			blue:setFillColor(0, 0, 1)

			local green = display.newRect(snap.group, 140 - CX, 210 - CY, 200, 100)

			green:setFillColor(0, 1, 0)

			-- Add some controls.
			snap.fill.effect = name

			utils.SliderAndText_Kernel(snap, 10, "extend", kernel)
			utils.SliderAndText_Kernel(snap, 40, "frequency", kernel)

			-- Update the snapshot.
			function group.m_func (event)
				blue.y = BlueY + sin(event.time / 1050) * 25

				snap:invalidate()
			end

			Groups[#Groups + 1] = group
		end,

		-- Effect 3 --
		"No Discard", "filter.heat.no_discard", AuxHeat(),

		-- Effect 4 --
		"Texel", "filter.heat.texel", AuxHeat(function(image)
			image.fill.effect.width = image.contentWidth
		end),

		-- Effect 5 --
		"Texel (No Discard)", "filter.heat.texel_no_discard", AuxHeat(function(image)
			image.fill.effect.width = image.contentWidth
		end)
	}, self.view)
end

Scene:addEventListener("create")

-- Show Scene --
function Scene:show (event)
	if event.phase == "did" then
		Runtime:addEventListener("enterFrame", EnterFrame)
	end
end

Scene:addEventListener("show")

-- Hide Scene --
function Scene:hide (event)
	if event.phase == "did" then
		Runtime:removeEventListener("enterFrame", EnterFrame)
	end
end

Scene:addEventListener("hide")

--
return Scene