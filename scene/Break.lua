--- "Break" shader demo scene.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Standard library imports --
local floor = math.floor
local pi = math.pi
local random = math.random
local sin = math.sin

-- Modules --
local unit_pair = require("corona_shader.encode.vars.unit_pair")
local utils = require("utils")

-- Corona globals --
local display = display
local transition = transition

-- Corona modules --
local composer = require("composer")

-- Break scene --
local Scene = composer.newScene()

-- --
local CX, CY = display.contentCenterX, display.contentCenterY

-- --
local NCols, NRows = 5, 5

-- --
local ScaleX, ScaleY = display.contentScaleX, display.contentScaleY

--
local function AddW (x, w)
	return x / w
end

--
local function AddH (y, h)
	return y / h
end

--
local function GetInnerPos (coord, dim)
	return floor(coord + .2 * (random() - .5) * dim)
end

-- Create Scene --
function Scene:create ()
	--
	local function AuxBreak (after)
		return function(group, name, kernel)
			local n, ys, ibounds, iw, ih = 0, {}

			for row = 1, NRows do
				local x3, x4, y3, y

				for col = 1, NCols do
					local image = display.newImageRect(group, "Image1.jpg", 256, 256)

					image.x, image.y = CX, CY

					if not ibounds then
						ibounds = image.contentBounds
						iw, ih = (ibounds.xMax - ibounds.xMin) / NCols, (ibounds.yMax - ibounds.yMin) / NRows
					end

					local path, cur = image.path, ys[col] or {}

					if x3 then
						path.x1, path.x2 = x4 - ibounds.xMin, x3 - ibounds.xMin
					end

					if col < NCols then
						local x = ibounds.xMin + col * iw

						x3 = GetInnerPos(x, iw)

						if row > 1 then
							x4 = cur.x3
						else
							x4 = GetInnerPos(x, iw)
						end				

						cur.x3, path.x3, path.x4 = x3, x3 - ibounds.xMax, x4 - ibounds.xMax
					end

					if row > 1 then
						path.y1, path.y4 = cur.y2 - ibounds.yMin, cur.y3 - ibounds.yMin
					end

					if row < NRows then
						y = y or ibounds.yMin + row * ih

						if y3 then
							cur.y2 = y3
						else
							cur.y2 = GetInnerPos(y, ih)
						end

						y3 = GetInnerPos(y, ih)

						cur.y3, path.y2, path.y3 = y3, cur.y2 - ibounds.yMax, y3 - ibounds.yMax
					end

					image.fill.effect = name

					local proxy, in_trans = { t = 0 }, after(image, ibounds, cur)

					transition.to(proxy, {
						t = 1, delta = true, time = random(1200, 3000), iterations = -1,

						transition = function(cur, time, start)
							local t = sin(pi * cur / time)

							in_trans(t^3)

							return t
						end
					})

					ys[col], n = cur, n + 1
				end
			end
		end
	end

	--
	local TexelFunc = AuxBreak(function(object, bounds)
		local angle, path = random(-90, 90), object.path
		local w = bounds.xMax - bounds.xMin
		local h = bounds.yMax - bounds.yMin

		object.fill.effect.c1 = unit_pair.Encode(AddW(path.x1, w), AddH(path.y1, h))
		object.fill.effect.c2 = unit_pair.Encode(AddW(path.x2, w), 1 + AddH(path.y2, h))
		object.fill.effect.c3 = unit_pair.Encode(1 + AddW(path.x3, w), 1 + AddH(path.y3, h))
		object.fill.effect.c4 = unit_pair.Encode(1 + AddW(path.x4, w), AddH(path.y4, h))

		return function(t)
			object.rotation = t * angle
		end
	end)

	-- Effect selection.
	local tb = utils.TabBar_Triples({
		-- Effect 1 --
		"Break", "filter.shard.basic", AuxBreak(function(object, bounds)
			object.fill.effect.min_x = bounds.xMin
			object.fill.effect.min_y = bounds.yMin
			object.fill.effect.max_x = bounds.xMax
			object.fill.effect.max_y = bounds.yMax

			local dx, dy = random(-10, 10), random(-10, 10)

			return function(t)
				local x = floor(CX + t * dx)
				local y = floor(CY + t * dy)

				object.x, object.y = x, y

				object.fill.effect.min_x = bounds.xMin + x - CX
				object.fill.effect.min_y = bounds.yMin + y - CY
				object.fill.effect.max_x = bounds.xMax + x - CX
				object.fill.effect.max_y = bounds.yMax + y - CY
			end
		end),

		-- Effect 2 --
		"Break (barycentric)", "filter.shard.barycentric", TexelFunc,

		-- Effect 3 --
		"Break (bilinear)", "filter.shard.bilinear", TexelFunc
	}, self.view)
end

Scene:addEventListener("create")

-- Show Scene --
function Scene:show (event)
	if event.phase == "did" then
	--	Runtime:addEventListener("enterFrame", EnterFrame)
	end
end

Scene:addEventListener("show")

-- Hide Scene --
function Scene:hide (event)
	if event.phase == "did" then
	--	Runtime:removeEventListener("enterFrame", EnterFrame)
	end
end

Scene:addEventListener("hide")

--
return Scene