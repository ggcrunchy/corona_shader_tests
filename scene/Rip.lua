--- Rip shader scene.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Modules --
local effect_props = require("corona_shader.effect_props")
local utils = require("utils")

-- Corona globals --
local display = display
local native = native

-- Corona modules --
local composer = require("composer")
local widget = require("widget")

-- Rip scene --
local Scene = composer.newScene()

-- --
local CX, CY = display.contentCenterX, display.contentCenterY

-- Create Scene --
function Scene:create ()
	--
	local back_group = display.newGroup()

	self.view:insert(back_group)

	--
	local function AuxRip (t)
		return function(group, name, kernel)
			local image = display.newImageRect(group, "Image1.jpg", 256, 256)

			image.x, image.y = CX, CY

			effect_props.AssignEffect(image, name)

			for i = 1, #t do
				utils.SliderAndText_Kernel(image, 10 + (i - 1) * 30, t[i], kernel)
			end

			utils.LineControls(image, "x1_y1", "x2_y2", "start.png", "end.png")

			--
			local back = display.newImageRect(back_group, "Image2.jpg", 256, 256)

			back.x, back.y = CX, CY
		end
	end

	-- Effect selection.
	local tb = utils.TabBar_Triples({
		-- Effect 1 --
		"Rip", "filter.rip.basic", AuxRip{ "distance", "width" },

		-- Effect 2 --
		"Rip (tapered)", "filter.rip.tapered", AuxRip{ "distance", "width" },

		-- Effect 2 --
		"Rip (tapered + noise)", "filter.rip.tapered_noise", AuxRip{ "distance", "frequency", "width" }
	}, self.view)

	--
	local text = display.newText(self.view, "Use background?", 0, tb.contentBounds.yMin - 15, native.systemFont, 24)

	text.anchorX, text.x = 0, 5

	local switch = widget.newSwitch{
		left = text.contentBounds.xMax + 3, style = "checkbox", initialSwitchState  = true,

		onPress = function(event)
			back_group.isVisible = event.target.isOn
		end
	}

	switch.y = text.y

	self.view:insert(switch)
end

Scene:addEventListener("create")

-- Show Scene --
function Scene:show (event)
	if event.phase == "did" then
	--	Runtime:addEventListener("enterFrame", EnterFrame)
	end
end

Scene:addEventListener("show")

-- Hide Scene --
function Scene:hide (event)
	if event.phase == "did" then
	--	Runtime:removeEventListener("enterFrame", EnterFrame)
	end
end

Scene:addEventListener("hide")

--
return Scene