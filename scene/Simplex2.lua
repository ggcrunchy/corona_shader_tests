--- Simplex noise shader scene.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Modules --
local utils = require("utils")

-- Corona globals --
local display = display

-- Corona modules --
local composer = require("composer")

-- Simplex noise scene --
local Scene = composer.newScene()

-- --
local CX, CY = display.contentCenterX, display.contentCenterY

-- Create Scene --
function Scene:create ()
	self.view:insert(utils.LoadKernel("generator.simplex2.basic", function(group, name, kernel)
		local shape = display.newCircle(group, CX, CY, 125)

		shape.fill.effect = name

		utils.SliderAndText_Kernel(shape, 10, "scale", kernel)
		utils.SliderAndText_Kernel(shape, 40, "speed", kernel)
		utils.SliderAndText_Kernel(shape, 70, "frequency", kernel)
	end))
end

Scene:addEventListener("create")

-- Show Scene --
function Scene:show (event)
	if event.phase == "did" then
	--	Runtime:addEventListener("enterFrame", EnterFrame)
	end
end

Scene:addEventListener("show")

-- Hide Scene --
function Scene:hide (event)
	if event.phase == "did" then
	--	Runtime:removeEventListener("enterFrame", EnterFrame)
	end
end

Scene:addEventListener("hide")

--
return Scene