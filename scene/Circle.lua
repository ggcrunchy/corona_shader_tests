--- Circle shader scene.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Standard library imports --
local ipairs = ipairs
local random = math.random
local sqrt = math.sqrt

-- Modules --
local utils = require("utils")

-- Corona globals --
local display = display
local Runtime = Runtime
local transition = transition

-- Corona modules --
local composer = require("composer")

-- Sphere scene --
local Scene = composer.newScene()

-- --
local CX, CY = display.contentCenterX, display.contentCenterY

-- --
local EnterFrame

-- Create Scene --
function Scene:create ()
	--
	local function AuxCircle (t)
		return function(group, name, kernel)
			local image = display.newCircle(group, CX, CY, 125)

			image.x, image.y = CX, CY
			image.fill = { type = "image", filename = "Image1.jpg" }
			image.fill.effect = name

			if t then
				for i, param in ipairs(t) do
					utils.SliderAndText_Kernel(image, 10 + (i - 1) * 30, param, kernel)
				end

				if t.has_control then
					utils.SingleControl(image, "light", "light.png")
				end
			end
		end
	end

	-- Effect selection.
	utils.TabBar_Triples({
		-- Effect 1 --
		"Circle", "filter.circle.basic", AuxCircle(),

		-- Effect 2 --
		"Bumped", "filter.circle.bumped", AuxCircle{ "distance", has_control = true }
	}, self.view)
end

Scene:addEventListener("create")

-- Show Scene --
function Scene:show (event)
	if event.phase == "did" then
	--	Runtime:addEventListener("enterFrame", EnterFrame)
	end
end

Scene:addEventListener("show")

-- Hide Scene --
function Scene:hide (event)
	if event.phase == "did" then
	--	Runtime:removeEventListener("enterFrame", EnterFrame)
	end
end

Scene:addEventListener("hide")

--
return Scene