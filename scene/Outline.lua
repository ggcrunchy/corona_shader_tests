--- Outline shader scene.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Modules --
local color_picker = require("corona_ui.widgets.color_picker")
local net = require("corona_ui.patterns.net")
local utils = require("utils")

-- Corona globals --
local display = display

-- Corona modules --
local composer = require("composer")

-- Outline scene --
local Scene = composer.newScene()

-- --
local CX, CY = display.contentCenterX, display.contentCenterY

-- Create Scene --
function Scene:create ()
	--
	local function AuxOutline (set_fill_color)
		return function(group, name, kernel)
			local image = display.newImageRect(group, "Image3.png", 256, 256)

			image.x, image.y = CX, CY
			image.fill.effect = name

			utils.SliderAndText_Kernel(image, 10, "thickness", kernel)

			local cp = color_picker.ColorPicker_XY(group, "center", "center", "100%", "40%")

			cp.isVisible = false

			local color = display.newRect(group, 50, group[group.numChildren - 2].contentBounds.yMax + 50, 50, 50)

			color:addEventListener("touch", function(event)
				if event.phase == "ended" or event.phase == "cancelled" then
					net.AddNet_Hide(group, cp)

					cp.isVisible = true
				end

				return true
			end)
			cp:addEventListener("color_change", function(event)
				local r, g, b = event.r, event.g, event.b

				color:setFillColor(r, g, b)

				if set_fill_color then
					image:setFillColor(r, g, b)
				else
					image.fill.effect.r = r
					image.fill.effect.g = g
					image.fill.effect.b = b
				end
			end)
		end
	end

	-- Effect selection.
	utils.TabBar_Triples({
		-- Effect 1 --
		"Outline", "filter.outlined.around", AuxOutline(false),

		-- Effect 2 --
		"Outline (only)", "filter.outlined.only", AuxOutline(true),
	}, self.view)
end

Scene:addEventListener("create")

-- Show Scene --
function Scene:show (event)
	if event.phase == "did" then
	--	Runtime:addEventListener("enterFrame", EnterFrame)
	end
end

Scene:addEventListener("show")

-- Hide Scene --
function Scene:hide (event)
	if event.phase == "did" then
	--	Runtime:removeEventListener("enterFrame", EnterFrame)
	end
end

Scene:addEventListener("hide")

--
return Scene