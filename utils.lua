--- Just some helper utilities for Corona shaders.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Standard library imports --
local abs = math.abs
local ipairs = ipairs
local loaded = package.loaded
local require = require

-- Modules --
local effect_props = require("corona_shader.effect_props")
local unit_pair = require("corona_shader.encode.vars.unit_pair")

-- Corona globals --
local display = display
local graphics = graphics
local native = native
local system = system
local transition = transition

-- Corona modules --
local widget = require("widget")

-- Cached module references --
local _LoadKernel_
local _SliderAndText_
local _TabBar_

-- Exports --
local M = {}

--- Helper to load kernels
function M.LoadKernel (effect_name, func)
	local resolved = "kernel." .. effect_name:gsub("custom%.", "")
	local kernel = loaded[resolved]

	if not kernel then
		kernel = require(resolved)

		graphics.defineEffect(kernel)
	end

	local group = display.newGroup()

	func(group, effect_name, kernel)

	return group
end

local FadeParams = {}

--
local function TouchCircle (func)
	local xoff, yoff

	return function(event)
		local phase, image = event.phase, event.target

		if phase == "began" then
			display:getCurrentStage():setFocus(image)

			xoff, yoff, FadeParams.alpha = event.x - image.x, event.y - image.y, 1

			transition.to(image, FadeParams)
		elseif phase == "moved" and xoff then
			image.x, image.y = func(event.x - xoff, event.y - yoff)
		elseif phase == "ended" or phase == "cancelled" then
			display.getCurrentStage():setFocus(nil)

			FadeParams.alpha, xoff, yoff = .35

			transition.to(image, FadeParams)
		end

		return true
	end
end

-- --
local Controls = {}

--
local function AuxLineControl (group, func, x, y, filename)
	local image = display.newImageRect(group, filename, 64, 64)

	image:addEventListener("touch", TouchCircle(func))

	image.alpha, image.x, image.y = .35, func(x, y)

	Controls[#Controls + 1] = image
end

--
local function Bounce (when, half)
	return half - abs(half - when % (2 * half))
end

--
Runtime:addEventListener("enterFrame", function(event)
	local angle, n = Bounce(event.time / 1000 * 95, 25), #Controls

	for i = n, 1, -1 do
		local image = Controls[i]

		if display.isValid(image) then
			image.rotation = angle
		else
			Controls[i] = Controls[n]
			n, Controls[n] = n - 1
		end
	end
end)

--
local function FitVar (v, min, max)
	if v < min then
		return min, 0
	elseif v > max then
		return max, 1
	else
		return v, (v - min) / (max - min)
	end
end

--
local function UpdateLine (target, line, x1, y1, x2, y2)
	display.remove(line)

	line = display.newLine(target.parent, x1, y1, x2, y2)

	line:setStrokeColor(0, 1, 0, .7)

	line.strokeWidth = 3

	return line
end

--
local function AuxControls (target, prop1, prop2, image1, image2, use_line)
	local tbounds, line = target.contentBounds
	local minx, miny, maxx, maxy = tbounds.xMin, tbounds.yMin, tbounds.xMax, tbounds.yMax
	local dx, dy = maxx - minx, maxy - miny

	--
	local x1, y1 = unit_pair.Decode(effect_props.GetEffectProperty(target, prop1))
	local x2, y2

	AuxLineControl(target.parent, function(x, y)
		x, x1 = FitVar(x, minx, maxx)
		y, y1 = FitVar(y, miny, maxy)

		effect_props.SetEffectProperty(target, prop1, unit_pair.Encode(x1, y1))

		if use_line and x2 then
			line = UpdateLine(target, line, x, y, minx + x2 * dx, miny + y2 * dy)
		end

		return x, y
	end, minx + x1 * dx, miny + y1 * dy, image1)

	--
	if prop2 then
		x2, y2 = unit_pair.Decode(effect_props.GetEffectProperty(target, prop2))

		AuxLineControl(target.parent, function(x, y)
			x, x2 = FitVar(x, minx, maxx)
			y, y2 = FitVar(y, miny, maxy)

			effect_props.SetEffectProperty(target, prop2, unit_pair.Encode(x2, y2))

			line = use_line and UpdateLine(target, line, minx + x1 * dx, miny + y1 * dy, x, y)

			return x, y
		end, minx + x2 * dx, miny + y2 * dy, image2)
	end
end

--- DOCME
function M.LineControls (target, prop1, prop2, image1, image2)
	AuxControls(target, prop1, prop2, image1, image2, true)
end

--- DOCME
function M.LinelessControls (target, prop1, prop2, image1, image2)
	AuxControls(target, prop1, prop2, image1, image2, false)
end

--- DOCME
function M.SingleControl (target, prop, image)
	AuxControls(target, prop, nil, image, nil, false)
end

-- Make a slider with some text to its right
function M.SliderAndText (target, top, prop, min_value, max_value)
	local group, range = target.parent, max_value - min_value -- see the shader kernel for these values
	local slider = widget.newSlider{
		top = top, left = 10,
		orientation = "horizontal",
		value = 100 * (effect_props.GetEffectProperty(target, prop) - min_value) / range,
		listener = function(event)
			effect_props.SetEffectProperty(target, prop, min_value + (event.value / 100) * range)
		end
	}

	group:insert(slider)

	local text = display.newText(group, prop, 0, slider.y, native.systemFont, 20)

	text.anchorX, text.x = 0, slider.contentBounds.xMax + 10
end

--
local function FindBounds (vdata, prop)
	for i = 1, #(vdata or "") do
		local param = vdata[i]

		if param.name == prop then
			return param.min, param.max
		end
	end
end

--- DOCME
function M.SliderAndText_Kernel (target, top, prop, kernel)
	local _, min_value, max_value = nil, FindBounds(kernel.vertexData, prop) -- todo: uniforms?

	if not min_value then
		_, min_value, max_value = effect_props.FoundInProperties(effect_props.GetName(kernel), prop)
	end

	return _SliderAndText_(target, top, prop, min_value, max_value)
end

--- Make a very basic tab bar
function M.TabBar (funcs, view)
	local buttons = {}

	for i, func in ipairs(funcs) do
		buttons[#buttons + 1] = { 
			label = func.name,
			selected = i == 1,
			onPress = func.func
		}
	end

	local tb = widget.newTabBar{
		top = display.contentHeight - 52,
		width = display.contentWidth,
		buttons = buttons
	}

	if view then
		view:insert(tb)
	end

	funcs[1].func()

	return tb
end

--- DOCME
function M.TabBar_Triples (gtriples, view)
	local funcs, current = {}

	for i = 1, #gtriples, 3 do
		local group = _LoadKernel_(gtriples[i + 1], gtriples[i + 2])

		group.isVisible = false

		if view then
			view:insert(group)
		end

		funcs[#funcs + 1] = {
			name = gtriples[i],
			func = function()
				if current then -- handle first time
					current.isVisible = false
				end

				current, group.isVisible = group, true
			end
		}
	end

	return _TabBar_(funcs, view)
end

-- Install some theme.
if system.getInfo("platform") == "Android" then
	widget.setTheme("widget_theme_android")
else
	widget.setTheme("widget_theme_ios")
end

-- Cache module members.
_LoadKernel_ = M.LoadKernel
_SliderAndText_ = M.SliderAndText
_TabBar_ = M.TabBar

-- Export the module.
return M