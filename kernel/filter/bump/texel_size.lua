--- A bump map shader variant.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Modules --
local loader = require("corona_shader.loader")

-- Kernel --
local kernel = { language = "glsl", category = "filter", group = "bump", name = "texel_size" }

kernel.fragment = loader.FragmentShader[[
	P_COLOR vec4 FragmentKernel (P_UV vec2 uv)
	{
		P_COLOR vec3 tcolor = texture2D(CoronaSampler0, uv).rgb;
		P_COLOR vec3 right = GetRightPixel(CoronaSampler0, uv).rgb;
		P_COLOR vec3 above = GetAbovePixel(CoronaSampler0, uv).rgb;
		P_COLOR float rz = dot(right - tcolor, vec3(1.));
		P_COLOR float uz = dot(above - tcolor, vec3(1.));

		P_POSITION vec3 rvec = vec3(CoronaTexelSize.x, 0., rz);
		P_POSITION vec3 uvec = vec3(0., CoronaTexelSize.y, uz);

		P_COLOR vec3 normal = normalize(cross(rvec, uvec)) * .5 + .5;

		return vec4(normal, 1.);
	}
]]

return kernel
