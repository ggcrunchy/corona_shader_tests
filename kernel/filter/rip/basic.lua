--- A "rip" shader.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Modules --
local loader = require("corona_shader.loader")
local unit_pair = require("corona_shader.encode.vars.unit_pair")

-- Kernel --
local kernel = { language = "glsl", category = "filter", group = "rip", name = "basic" }

-- Expose effect parameters using vertex data
kernel.vertexData = {
	{
		name = "distance",
		default = 0, 
		min = 0,
		max = 1,
		index = 0
	},

	{
		name = "width",
		default = .2,
		min = .005,
		max = math.sqrt(2),
		index = 1
	},

	unit_pair.VertexDatum("x1_y1", 2, 0, 0),
	unit_pair.VertexDatum("x2_y2", 3, 1, 1)
}

kernel.vertex = loader.VertexShader[[
	varying P_POSITION vec2 v_Diff;
	varying P_POSITION vec2 v_Seg;
	varying P_POSITION float v_Dist;

	P_POSITION vec2 VertexKernel (P_POSITION vec2 position)
	{
		P_POSITION vec2 P = UnitPair(CoronaVertexUserData.z); // z = (x1, y1)

		v_Diff = CoronaTexCoord - P;
		v_Seg = UnitPair(CoronaVertexUserData.w) - P; // w = (x2, y2)

		P_UV float scale = -2. * step(0., -length(v_Seg)) + 1.;

		v_Dist = CoronaVertexUserData.x * scale; // x = distance

		return position;
	}
]]

kernel.fragment = [[
	varying P_POSITION vec2 v_Diff;
	varying P_POSITION vec2 v_Seg;
	varying P_POSITION float v_Dist;

	P_COLOR vec4 FragmentKernel (P_UV vec2 uv)
	{
		P_POSITION float pos = dot(v_Diff, v_Seg) / dot(v_Seg, v_Seg);

		if (pos >= 0. && pos <= v_Dist && length(v_Diff - pos * v_Seg) <= CoronaVertexUserData.y) return vec4(0.); // y = width

		return CoronaColorScale(texture2D(CoronaSampler0, uv));
	}
]]

return kernel
