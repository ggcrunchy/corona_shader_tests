--- An outlined shader, adapted from [this](http://blogs.love2d.org/content/let-it-glow-dynamically-adding-outlines-characters).

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Modules --
local loader = require("corona_shader.loader")

-- Kernel --
local kernel = { language = "glsl", category = "filter", group = "outlined", name = "around" }

-- Expose effect parameters using vertex data
kernel.vertexData = {
	{
		name = "thickness",
		default = 1, 
		min = .5,
		max = 7,
		index = 0
	},

	{
		name = "r",
		default = 1, 
		min = 0,
		max = 1,
		index = 1
	},

	{
		name = "g",
		default = 1, 
		min = 0,
		max = 1,
		index = 2
	},

	{
		name = "b",
		default = 1, 
		min = 0,
		max = 1,
		index = 3
	}
}

kernel.fragment = loader.FragmentShader[[
	P_COLOR vec4 FragmentKernel (P_UV vec2 uv)
	{
		P_UV vec4 tcolor = texture2D(CoronaSampler0, uv);
		P_UV float alpha = GetLaplacian(CoronaSampler0, uv, tcolor.a, CoronaVertexUserData.x);

		tcolor.rgb -= CoronaVertexUserData.yzw * alpha * (1. - tcolor.a);

		return CoronaColorScale(tcolor);
	}
]]

return kernel