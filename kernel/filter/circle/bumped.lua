--- A texture-mapped circle shader with (internally generated) bump mapping.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Modules --
local loader = require("corona_shader.loader")
local unit_pair = require("corona_shader.encode.vars.unit_pair")

-- Kernel --
local kernel = { language = "glsl", category = "filter", group = "circle", name = "bumped" }

-- Expose effect parameters using vertex data
kernel.vertexData = {
	{
		name = "distance",
		default = 2, 
		min = 1,
		max = 5,
		index = 0
	},

	unit_pair.VertexDatum("light", 1, .3, .6)
}

kernel.isTimeDependent = true

kernel.vertex = loader.VertexShader[[
	varying P_UV vec2 pos;

	P_POSITION vec2 VertexKernel (P_POSITION vec2 position)
	{
		pos = 2. * UnitPair(CoronaVertexUserData.y) - 1.; // y = (light x, light y)

		return position;
	}
]]

kernel.fragment = loader.FragmentShader{
	prelude = [[
		#define SPHERE_NO_DISCARD
	]],	main = [[
		varying P_UV vec2 pos;

		P_COLOR vec4 FragmentKernel (P_UV vec2 uv)
		{
			P_UV vec2 diff = 2. * uv - 1.;
			P_UV vec4 uv_zp = GetUV_PhiDelta_ZPhi(diff, -CoronaTotalTime / 7.);

			if (uv_zp.s < 0.) return vec4(0.);

			P_COLOR vec4 tcolor = texture2D(CoronaSampler0, uv_zp.xy);
			P_COLOR vec3 bump = ComputeNormal(CoronaSampler0, uv_zp.xy, tcolor.rgb);
			P_POSITION vec3 N = vec3(diff, uv_zp.z);
			P_POSITION vec3 wn = GetWorldNormal(bump, GetTangent(diff, uv_zp.w), N);
			P_POSITION vec3 L = normalize(vec3(pos, CoronaVertexUserData.x) - N); // x = distance
			P_COLOR vec3 nl = min(.2 + tcolor.rgb * max(dot(wn, L), 0.), 1.);

			return CoronaColorScale(vec4(nl, 1.));
		}
	]]
}

return kernel
