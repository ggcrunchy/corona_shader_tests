--- A texture-mapped circle shader.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Modules --
local loader = require("corona_shader.loader")

-- Kernel --
local kernel = { language = "glsl", category = "filter", group = "circle", name = "basic" }

kernel.isTimeDependent = true

kernel.fragment = loader.FragmentShader{
	prelude = [[
		#define SPHERE_NO_DISCARD
	]], main = [[
		P_COLOR vec4 FragmentKernel (P_UV vec2 uv)
		{
			uv = GetUV_PhiDelta(2. * uv - 1., -CoronaTotalTime / 7.);

			if (uv.s < 0.) return vec4(0.);

			return CoronaColorScale(texture2D(CoronaSampler0, uv));
		}
	]]
}

return kernel
