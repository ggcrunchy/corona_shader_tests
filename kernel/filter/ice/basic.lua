--- An ice shader, adapted from Anders Nivfors's article, "Rendering Realistic Ice Objects",
-- in ShaderX6.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Modules --
local loader = require("corona_shader.loader")
local unit_pair = require("corona_shader.encode.vars.unit_pair")

-- Kernel --
local kernel = { language = "glsl", category = "filter", group = "ice", name = "basic" }

-- Expose effect parameters using vertex data
kernel.vertexData = {
	{
		name = "eta",
		default = 1.3091, 
		min = 0,
		max = 5,
		index = 0
	},

	-- spec_strength (1), set below: [1, 80; def = 20], [0, 1; def = .5]

	unit_pair.VertexDatum("eye", 2, .2, .4),
	unit_pair.VertexDatum("light", 3, .1, .4)
}

unit_pair.AddVertexProperty(kernel, 1, "spec", "strength", "spec_strength", (20 - 1) / (80 - 1), .5)

kernel.vertex = loader.VertexShader[[
	varying P_POSITION vec2 v_Eye;
	varying P_POSITION vec2 v_Light;
	varying P_POSITION float v_Spec;
	varying P_POSITION float v_Strength;

	P_POSITION vec2 VertexKernel (P_POSITION vec2 position)
	{
		v_Eye = UnitPair(CoronaVertexUserData.z) - CoronaTexCoord; // z = (eye x, eye y)
		v_Light = UnitPair(CoronaVertexUserData.w) - CoronaTexCoord; // w = (light x, light y)

		P_POSITION vec2 spec_strength = UnitPair(CoronaVertexUserData.y); // y = (specular coeff, strength)

		v_Spec = spec_strength.x;
		v_Strength = spec_strength.y;

		return position;
	}
]]

kernel.fragment = loader.FragmentShader[[
	varying P_POSITION vec2 v_Eye;
	varying P_POSITION vec2 v_Light;
	varying P_POSITION float v_Spec;
	varying P_POSITION float v_Strength;

	P_COLOR vec3 EnvMap (P_UV vec3 v)
	{
		P_UV vec2 uv = GetUV(v);
//		return vec3(IQ(uv * (sin(CoronaTotalTime * 2.3) * .7 + 2.1)), IQ(uv * 3.7), IQ(uv * 2.1));
		return texture2D(CoronaSampler0, GetUV(v)).rgb; // Ideally, would look into a cubemap
	}

	P_COLOR vec4 FragmentKernel (P_UV vec2 uv)
	{
		P_COLOR vec3 bn = ComputeNormal(CoronaSampler0, uv);
		P_POSITION vec3 wn = normalize(vec3(0., 0., 1.) - bn * v_Strength);
		P_POSITION vec3 eye = normalize(vec3(v_Eye, 1.));
		P_POSITION vec3 light = normalize(vec3(v_Light, 1.));

		P_UV vec3 reflectv = reflect(eye, wn);
		P_COLOR vec3 reflectc = EnvMap(reflectv);

		P_UV vec3 refractv = refract(eye, wn, CoronaVertexUserData.x); // x = eta
		P_COLOR vec3 refractc = EnvMap(refractv);

		P_POSITION float spec = pow(clamp(dot(light, -reflectv), 0., 1.), 1. + v_Spec * 79.);
		P_POSITION float fres = 1. - abs(dot(eye, wn));

		P_COLOR vec4 base = CoronaColorScale(vec4(mix(refractc, reflectc, fres * fres * fres), 1.));

		return vec4(min((base.rgb + vec3(.5 * bn.r + .5) * 0.16) * 0.88 + spec, 1.), base.a);
	}
]]

return kernel