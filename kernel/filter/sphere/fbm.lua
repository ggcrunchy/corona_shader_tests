--- A texture-mapped sphere shader.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Modules --
local loader = require("corona_shader.loader")

-- Kernel --
local kernel = { language = "glsl", category = "filter", group = "sphere", name = "fbm" }

kernel.vertexData = {
	{
		name = "x",
		default = 0, 
		min = -1,
		max = 1,
		index = 0
	},

	{
		name = "y",
		default = 0, 
		min = -1,
		max = 1,
		index = 1
	}
}

kernel.fragment = loader.FragmentShader[[
	P_COLOR vec4 FragmentKernel (P_UV vec2 uv)
	{
		P_POSITION vec3 diff = vec3(2. * uv - 1., 0.);
		P_POSITION float dist_sq = dot(diff.xy, diff.xy);

		if (dist_sq > 1.) return vec4(0.);

		diff.z = sqrt(1. - dist_sq);

		// Copyright (c) Stefan Gustavson 2011-04-19. All rights reserved.
		// This code is released under the conditions of the MIT license.
		// See LICENSE (above)

		diff.xy += CoronaVertexUserData.xy;

		P_UV float n = abs(Simplex3(diff));

		n += .5 * abs(Simplex3(diff * 2.));
		n += .25 * abs(Simplex3(diff * 4.));
		n += .125 * abs(Simplex3(diff * 8.));
  
		return CoronaColorScale(vec4(vec3(1.5 - n, 1. - n, .5 - n), 1.));
	}
]]

return kernel