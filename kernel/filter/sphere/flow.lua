--- A texture-mapped sphere shader.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Modules --
local loader = require("corona_shader.loader")

-- Kernel --
local kernel = { language = "glsl", category = "filter", group = "sphere", name = "flow" }

kernel.isTimeDependent = true

kernel.fragment = loader.FragmentShader[[
	P_COLOR vec4 FragmentKernel (P_UV vec2 uv)
	{
		P_POSITION vec2 diff = 2. * uv - 1.;

		if (dot(diff, diff) > 1.) return vec4(0.);

		// GLSL implementation of 2D "flow noise" as presented
		// by Ken Perlin and Fabrice Neyret at Siggraph 2001.
		// (2D simplex noise with analytic derivatives and
		// in-plane rotation of generating gradients,
		// in a fractal sum where higher frequencies are
		// displaced (advected) by lower frequencies in the
		// direction of their gradient. For details, please
		// refer to the 2001 paper "Flow Noise" by Perlin and Neyret.)
		//
		// Author: Stefan Gustavson (stefan.gustavson@liu.se)
		// Distributed under the terms of the MIT license.
		// See LICENSE (above)

		P_UV vec2 g1, g2;
		P_UV float n1 = SimplexRD2(uv * .5, .2 * CoronaTotalTime, g1);
		P_UV float n2 = SimplexRD2(uv * 2.0 + g1 * .5, .51 * CoronaTotalTime, g2);
		P_UV float n3 = SimplexRD2(uv * 4.0 + g1 * .5 + g2 * .25, .77 * CoronaTotalTime, g2);

		return CoronaColorScale(vec4(vec3(.4, .5, .6) + vec3(n1 + .75 * n2 + .5 * n3), 1.0));
	}
]]

return kernel