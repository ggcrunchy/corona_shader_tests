--- Shard shader, using barycentric coordinates.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Modules --
local loader = require("corona_shader.loader")
local unit_pair = require("corona_shader.encode.vars.unit_pair")

-- Kernel --
local kernel = { language = "glsl", category = "filter", group = "shard", name = "barycentric" }

-- Expose effect parameters using vertex data
kernel.vertexData = {
	unit_pair.VertexDatum("c1", 0, 0, 0),
	unit_pair.VertexDatum("c2", 1, 1, 0),
	unit_pair.VertexDatum("c3", 2, 0, 1),
	unit_pair.VertexDatum("c4", 3, 1, 1)
}

kernel.vertex = loader.VertexShader[[
	varying P_UV vec4 x;
	varying P_UV vec4 y;
	varying P_UV vec4 bc;

	P_POSITION vec2 VertexKernel (P_POSITION vec2 position)
	{
		UnitPair4_OutM(CoronaVertexUserData, x, y);

		bc = BaryPrep(min(CoronaTexCoord, 1.));

		return position;
	}
]]

kernel.fragment = loader.FragmentShader[[
	varying P_UV vec4 x;
	varying P_UV vec4 y;
	varying P_UV vec4 bc;

	P_COLOR vec4 FragmentKernel (P_UV vec2 uv)
	{
		P_UV vec4 bary = BaryApply(bc);

		return CoronaColorScale(texture2D(CoronaSampler0, vec2(dot(bary, x), dot(bary, y))));
	}
]]

return kernel