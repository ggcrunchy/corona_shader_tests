--- Shard shader.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Modules --
local loader = require("corona_shader.loader")

-- Kernel --
local kernel = { language = "glsl", category = "filter", group = "shard", name = "basic" }

-- Expose effect parameters using vertex data
kernel.vertexData = {
	{
		name = "min_x",
		default = 0, 
		min = -2 * display.contentWidth,
		max = 2 * display.contentWidth,
		index = 0
	},

	{
		name = "min_y",
		default = 0, 
		min = -display.contentHeight,
		max = display.contentHeight,
		index = 1
	},

	{
		name = "max_x",
		default = 0, 
		min = -2 * display.contentWidth,
		max = 2 * display.contentWidth,
		index = 2
	},

	{
		name = "max_y",
		default = 0, 
		min = -display.contentHeight,
		max = display.contentHeight,
		index = 3
	},
}

kernel.vertex = [[
	varying P_UV vec2 uv2;

	P_POSITION vec2 VertexKernel (P_POSITION vec2 position)
	{
		uv2 = (position - CoronaVertexUserData.xy) / (CoronaVertexUserData.zw - CoronaVertexUserData.xy);

		return position;
	}
]]

kernel.fragment = loader.FragmentShader[[
	varying P_UV vec2 uv2;

	P_COLOR vec4 FragmentKernel (P_UV vec2 uv)
	{
		return CoronaColorScale(texture2D(CoronaSampler0, uv2));
	}
]]

return kernel