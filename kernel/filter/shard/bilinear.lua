--- Shard shader, using bilinear interpolation.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Modules --
local loader = require("corona_shader.loader")
local unit_pair = require("corona_shader.encode.vars.unit_pair")

-- Kernel --
local kernel = { language = "glsl", category = "filter", group = "shard", name = "bilinear" }

-- Expose effect parameters using vertex data
kernel.vertexData = {
	unit_pair.VertexDatum("c1", 0, 0, 0),
	unit_pair.VertexDatum("c2", 1, 1, 0),
	unit_pair.VertexDatum("c3", 2, 0, 1),
	unit_pair.VertexDatum("c4", 3, 1, 1)
}

kernel.vertex = loader.VertexShader[[
	varying P_UV vec4 x;
	varying P_UV vec4 y;
	varying P_UV vec2 uv2;

	P_POSITION vec2 VertexKernel (P_POSITION vec2 position)
	{
		UnitPair4_OutM(CoronaVertexUserData, x, y);

		uv2 = min(CoronaTexCoord, 1.);

		return position;
	}
]]

kernel.fragment = loader.FragmentShader[[
	varying P_UV vec4 x;
	varying P_UV vec4 y;
	varying P_UV vec2 uv2;

	P_COLOR vec4 FragmentKernel (P_UV vec2 uv)
	{
		// Corona path order:
		// x[0], y[0] -> x[3], y[3]
		// x[1], y[1] -> x[2], y[2]
		P_UV vec4 left = vec4(x.xy, y.xy);
		P_UV vec4 right = vec4(x.wz, y.wz);

		// After first mix:
		// xy: top, bottom x
		// zw: top, bottom y
		P_UV vec4 top_bottom = mix(left, right, uv2.x);

		return CoronaColorScale(texture2D(CoronaSampler0, mix(top_bottom.xz, top_bottom.yw, uv2.y)));
	}
]]

return kernel