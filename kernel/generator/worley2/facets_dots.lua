--- An example shader that uses Worley noise.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Modules --
local loader = require("corona_shader.loader")

-- Kernel --
local kernel = { language = "glsl", category = "generator", group = "worley2", name = "facets_dots" }

kernel.isTimeDependent = true

kernel.vertexData = {
	{
		name = "scale",
		default = 25, 
		min = 10,
		max = 100,
		index = 0
	},

	{
		name = "speed",
		default = 2.7, 
		min = 0,
		max = 15,
		index = 1
	},

	{
		name = "frequency",
		default = 1.3, 
		min = .2,
		max = 8,
		index = 2
	}
}

kernel.fragment = loader.FragmentShader[[
	P_COLOR vec4 FragmentKernel (P_UV vec2 uv)
	{
		uv.st *= CoronaVertexUserData.x;
		uv.s += CoronaTotalTime * CoronaVertexUserData.y;
		uv.t *= sin(CoronaTotalTime * CoronaVertexUserData.z);

		// Cellular noise ("Worley noise") in 2D in GLSL.
		// Copyright (c) Stefan Gustavson 2011-04-19. All rights reserved.
		// This code is released under the conditions of the MIT license.
		// See LICENSE (above)
		P_UV vec2 F = Worley2(uv); 

		P_UV float facets = 0.1 + (F.y - F.x);
		P_UV float dots = smoothstep(0.05, 0.1, F.x);

		return CoronaColorScale(vec4(vec3(facets * dots), 1.));
	}
]]

return kernel
