This project is a demonstration of various [custom shader effects](http://docs.coronalabs.com/daily/guide/graphics/customEffects.html).

Some of these are just to experiment, while others will probably make their way into a package of some sort.

Until then, this is also a lab of sorts to work out some reasonable modules around Corona shaders.

## Requirements

This project requires daily build 2015.2558 or later.